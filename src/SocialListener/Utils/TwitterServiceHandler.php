<?php

namespace SocialListener\Utils;

use TwitterOAuth\Auth\SingleUserAuth;
use TwitterOAuth\Serializer\ArraySerializer;

class TwitterServiceHandler
{
    private $credentials = [];

    private $authUrl;

    private $tweetsCount;

    public function __construct($credentials, $authUrl = '', $tweetsCount = 25)
    {
        $this->credentials = $credentials;
        $this->authUrl = $authUrl;
        $this->tweetsCount = $tweetsCount;
    }

    public function requestAccess()
    {
        $auth = $this->getAuth();

        $response = $auth->post('oauth/request_token', [
            'oauth_callback' => $this->authUrl,
        ]);

        return $response['oauth_token'];
    }

    public function verifyToken($oauthVerifier, $oauthToken)
    {
        $auth = $this->getAuth();

        $response = $auth->post('oauth/access_token', [
            'oauth_token' => $oauthToken,
            'oauth_verifier' => $oauthVerifier,
        ]);

        return $response;
    }

    public function getHomeTimeline($token, $secret, $screenName)
    {
        $this->credentials['oauth_token'] = $token;
        $this->credentials['oauth_token_secret'] = $secret;

        $auth = $this->getAuth();

        $response = $auth->get('statuses/home_timeline', [
            'count' => $this->tweetsCount,
        ]);

        return $response;
    }

    private function getAuth()
    {
        return new SingleUserAuth($this->credentials, new ArraySerializer());
    }
}