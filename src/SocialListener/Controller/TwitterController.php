<?php

namespace SocialListener\Controller;

use Core\Controller;
use SocialListener\Utils\TwitterServiceHandler;

class TwitterController extends Controller
{
    public function authAction()
    {
//        if ($this->session->has('oauth_token') && $this->session->has('oauth_token_secret')) {
//            header('Location: /twitter/list/');
//        }

        /** @var TwitterServiceHandler $twitterService */
        $twitterService = $this->container->get('twitter.service');

        $oauthToken = $twitterService->requestAccess();

        $this->session->set('oauth_token', $oauthToken);

        header('Location: https://api.twitter.com/oauth/authenticate?oauth_token='. $oauthToken);
    }

    public function authSuccessAction($response)
    {
        $oauthVerified = htmlspecialchars($_GET['oauth_verifier']);
        $oauthToken = $this->session->get('oauth_token');

        /** @var TwitterServiceHandler $twitterService */
        $twitterService = $this->container->get('twitter.service');

        $result  = $twitterService->verifyToken($oauthVerified, $oauthToken);

        $this->session->set('oauth_token', $result['oauth_token']);
        $this->session->set('oauth_token_secret', $result['oauth_token_secret']);
        $this->session->set('screen_name', $result['screen_name']);

        header('Location: /twitter/list/');
    }

    public function listAction()
    {
        if (!$this->session->has('oauth_token') || !$this->session->has('oauth_token_secret')) {
            header('Location: /twitter/auth/');
        }

        $tweets = $this->getTweets();

        return $this->twig->render('twitter/home-timeline.html.twig', [
            'tweets' => json_encode($tweets)
        ]);
    }

    public function listUpdateAction()
    {
        if (!$this->session->has('oauth_token') || !$this->session->has('oauth_token_secret')) {
            return false;
        }

        $tweets = $this->getTweets();

        return json_encode($tweets);
    }

    private function getTweets()
    {
        $token = $this->session->get('oauth_token');
        $secret = $this->session->get('oauth_token_secret');
        $screenName = $this->session->get('screen_name');

        /** @var TwitterServiceHandler $twitterService */
        $twitterService = $this->container->get('twitter.service');

        return $twitterService->getHomeTimeline($token, $secret, $screenName);
    }
}

