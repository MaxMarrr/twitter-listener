<?php

namespace SocialListener\Controller;

use Core\Controller;

class WebController extends Controller
{
    public function indexAction()
    {
        return $this->twig->render('index.html.twig');
    }

    public function notFoundAction()
    {
        return $this->twig->render('error-not-found.html.twig');
    }

    public function notAllowedAction()
    {
        return $this->twig->render('error-not-allowed.html.twig');
    }
}

