<?php

namespace Core;

abstract class Singleton
{
    protected static $instance = [];

    public static function getInstance()
    {
        $class = get_called_class();

        if ( ! isset(self::$instance[$class])) {
            self::$instance[$class] = new $class();
        }

        return self::$instance[$class];
    }

    protected function __clone() {}
    protected function __construct() {}
}