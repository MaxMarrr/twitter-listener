<?php

namespace Core;

use DI\Container;

class DIContainer extends Singleton
{
    /**
     * @var Container
     */
    private $container;

    public function set(Container $container)
    {
        $this->container = $container;
    }

    public function get()
    {
        return $this->container;
    }
}
