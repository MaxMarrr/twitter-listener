<?php

namespace Core;

use Twig_Environment;

class Controller
{
    /**
     * @var Twig_Environment
     */
    protected $twig;

    /**
     * @var SessionStorage
     */
    protected $session;

    /**
     * @var DIContainer
     */
    protected $container;

    function __construct(Twig_Environment $twig, SessionStorage $session)
    {
        $this->twig = $twig;
        $this->session = $session;
        $this->container = DIContainer::getInstance()->get();
    }
}