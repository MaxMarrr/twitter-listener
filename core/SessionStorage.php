<?php

namespace Core;

class SessionStorage extends Singleton
{
    public function set($key, $value)
    {
        $_SESSION[$key] = $value;
    }

    public function get($key)
    {
        if ( ! key_exists($key, $_SESSION)) {
            throw new \Exception('Session storage doesn\'t contain any value for this key.');
        }

        return $_SESSION[$key];
    }

    public function has($key)
    {
        return key_exists($key, $_SESSION);
    }
}
