<?php

use FastRoute\Dispatcher;
use Core\DIContainer;

$container  = require __DIR__ . '/../app/bootstrap.php';
$dispatcher = require __DIR__ . '/../app/config/routes.php';

DIContainer::getInstance()->set($container);

$route = $dispatcher->dispatch($_SERVER['REQUEST_METHOD'], $_SERVER['REQUEST_URI']);

switch ($route[0]) {
    case Dispatcher::NOT_FOUND:
        header('Location: /404/');
        break;

    case Dispatcher::METHOD_NOT_ALLOWED:
        header('Location: /405/');
        break;

    case Dispatcher::FOUND:
        $controller = $route[1];
        $parameters = $route[2];

        session_start();

        if (is_array($controller) && key_exists(1, $controller)) {
            $controller[1] .= 'Action';
        }

        echo $container->call($controller, $parameters);
        break;
}
