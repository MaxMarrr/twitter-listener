<?php

use Interop\Container\ContainerInterface;
use Symfony\Component\Yaml\Yaml;
use Core\SessionStorage;
use SocialListener\Utils\TwitterServiceHandler;

return [
    'params' => Yaml::parse(file_get_contents(__DIR__ . '/config.yml')),

    'twitter.service' => function (ContainerInterface $c) {
        $twitterParams = $c->get('params')['twitter'];

        $twitterCredentials = $twitterParams['credentials'];
        $authUrl = $twitterParams['authUrl'];
        $tweetsCount = $twitterParams['tweetsCount'];

        return new TwitterServiceHandler($twitterCredentials, $authUrl, $tweetsCount);
    },

    SessionStorage::class => function () {
        return SessionStorage::getInstance();
    },

    Twig_Environment::class => function () {
        $loader = new Twig_Loader_Filesystem(__DIR__ . '/../../src/SocialListener/Resources/views');
        return new Twig_Environment($loader);
    },
];