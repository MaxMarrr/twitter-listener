<?php

use FastRoute\RouteCollector;
use function FastRoute\simpleDispatcher;

$dispatcher = simpleDispatcher(function (RouteCollector $r) {
    $r->addRoute('GET', '/', ['SocialListener\Controller\WebController', 'index']);
    $r->addRoute('GET', '/404/', ['SocialListener\Controller\WebController', 'notFound']);
    $r->addRoute('GET', '/405/', ['SocialListener\Controller\WebController', 'notAllowed']);

    $r->addRoute('GET', '/twitter/auth/', ['SocialListener\Controller\TwitterController', 'auth']);
    $r->addRoute('GET', '/twitter/auth-success/{response}', ['SocialListener\Controller\TwitterController', 'authSuccess']);
    $r->addRoute('GET', '/twitter/list/', ['SocialListener\Controller\TwitterController', 'list']);
    $r->addRoute('GET', '/twitter/list/update/', ['SocialListener\Controller\TwitterController', 'listUpdate']);
});

return $dispatcher;